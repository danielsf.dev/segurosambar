
let listPatients = Array();                 //lista general de pacientes
let colors = ["rojo", "naranja", "verde"];  //lista de colores permitidos 
let listPatientsRed = Array();              //lista de pacientes color rojo
let listPatientsOrange = Array();           //lista de pacientes color naranja
let listPatientsGreen = Array();            //lista de pacientes color verte
let nTicketsRandom = 50;

function newTicket(color){  //funcion para generar un nuevo ticket
    console.log(color);     //imprimimos el color generado aleatoriamente
    listPatients = [];      //limpiamos la lista de pacientes
    
    var today = new Date(); // generamos una nueva fecha
    var date = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    if (color == colors[0]) //validamos si el color es rojo
        listPatientsRed.push({color: colors[0], dateTime: dateTime});
    else if (color == colors[1]) //validamos si el color es naranka
        listPatientsOrange.push({color: colors[1], dateTime: dateTime});
    else if (color == colors[2]) //validamos si el color es verde
        listPatientsGreen.push({color: colors[2], dateTime: dateTime});
    
    listPatientsRed.forEach(e => {listPatients.push(e);});      //insertamos los colores en la lista general de pacientes
    listPatientsOrange.forEach(e => {listPatients.push(e);});
    listPatientsGreen.forEach(e => {listPatients.push(e);});
}

function rand(min, max) {   //funcion para generar tickets randomn
    return Math.round(Math.random() * (max - min) + min); 
}

function init(){    //funcion principal para llamar el resto de funciones
    console.log("Colores generados");
    console.log("-----------------");
    for (let i = 0; i < nTicketsRandom; i++) 
        newTicket(colors[rand(0, 2)]);//create new random ticket
    console.log("-----------------\n");
    console.log("-----------------");
    console.log("Lista de pacientes por atender");
    console.log("-----------------");
    console.log(listPatients);//print result
    console.log("-----------------");
}

init(); //mandamos a llamar la funcion principal