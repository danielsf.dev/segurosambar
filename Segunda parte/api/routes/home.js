const express = require('express');
const jwtValidate = require('../middlewares/jwtCheck');
const router = express.Router();

router.get('/', jwtValidate, (req, res) => {
    res.send({message: "Hello home"});
});

module.exports = router;