const express = require('express');

const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/User');

router.post('/signin', (req, res) => {
    User.findOne({email: req.body.email }, (err, user)=>{
        if (err) 
            return res.status(500).json({success: false, message: err});
        if (!user)
            return res.status(401).json({success: false,message: "Usuario o contraseña incorrectos"});
        if (!bcrypt.compareSync(req.body.pass, user.pass))
            return res.status(401).json({success: false,message: "Usuario o contraseña incorrectos"});
        let token = jwt.sign({usuario: user,}, 'jwtKey', {expiresIn: '48h'});
        res.json({success: true, data: user, token,});
    });
});


router.post('/signup', (req, res) => {
    var { name, email, pass } = req.body;
    
    let newUser = new User({name, email, pass: bcrypt.hashSync(pass, 10)});
    newUser.save((err, user) => {
        if (err) 
            return res.status(400).json({success: false, message: err});
        res.status(201).json({success: true, data: user, message: "Registrado con exito"});
    });
});

module.exports = router;