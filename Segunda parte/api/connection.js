const mongoose = require('mongoose');
const uri = 'mongodb://162.241.99.66:27017/seguros';

mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).catch(err => console.log(err));

mongoose.connection.once('open', _ => {
    console.log('Database is connected to ', uri);
});

mongoose.connection.on('error', err => {
    console.log(err);
});

