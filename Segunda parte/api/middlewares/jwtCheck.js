const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const router = express.Router(); 

router.use((req, res, next) => {
    const token = req.headers.authorization;
    if (token) {
      jwt.verify(token, 'jwtKey', (err, decoded) => {      
        if (err) return res.json({ mensaje: 'Token incorrecto.' });    
        req.decoded = decoded;    
        next();
      });
    } else {
      res.send({ mensaje: 'Es necesario un token de autenticacion para continuar.' });
    }
 });

 module.exports = router;