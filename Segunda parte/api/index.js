const express = require('express');
var cors = require('cors')
const app = express();
require('./connection');
//const User = require('./models/User');

const authRoutes = require('./routes/auth');
const homeRoutes = require('./routes/home');
const bodyParser = require('body-parser')

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json())
app.use(cors())

//routes
app.use('/auth/', authRoutes);
app.use('/home/', homeRoutes);

//start the server
app.listen(3000, () => {
    console.log(`Server on port ${app.get('port')}`);
});