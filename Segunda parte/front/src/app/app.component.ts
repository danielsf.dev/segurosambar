import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { AuthService } from './services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { RoleGuardService } from './services/role-guard.service';
import { environment } from 'src/environments/environment';
//import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BookStore';
  cEmail: string;
  cName: string;

  linkRef: HTMLLinkElement;
  themes = [
    { name: 'light', href: 'https://unpkg.com/@clr/ui/clr-ui.min.css' },
    { name: 'dark', href: 'https://unpkg.com/@clr/ui/clr-ui-dark.min.css' }
  ];
  theme = this.themes[0];
  public innerWidth: any;
  isCollapsed: boolean = false;
  cVersion: string;


  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: Object,
    private authService: AuthService,
    private roleGuardService: RoleGuardService,
    private session: SessionStorageService,
    public router: Router
    // private translate: TranslateService
  ) {

    this.cVersion = environment.cVersion;
    //translate.setDefaultLang('en');

    this.innerWidth = window.innerWidth;

    if (isPlatformBrowser(this.platformId)) {
      try {
        const stored = localStorage.getItem('theme');
        if (stored) {
          this.theme = JSON.parse(stored);
        }
      } catch (err) {
        // Nothing to do
      }
      this.linkRef = this.document.createElement('link');
      this.linkRef.rel = 'stylesheet';
      this.linkRef.href = this.theme.href;
      this.document.querySelector('head').appendChild(this.linkRef);
    }
  }

  switchTheme() {
    if (this.theme.name === 'light') {
      this.theme = this.themes[1];
    } else {
      this.theme = this.themes[0];
    }
    localStorage.setItem('theme', JSON.stringify(this.theme));
    this.linkRef.href = this.theme.href;
  }


  // useLanguage(language: string) {
  //     this.translate.use(language);
  // }

  logOut() {
    this.session.clear();
    this.router.navigate(['/login']);
  }

  isAuth() {
    this.cEmail = this.session.retrieve('cEmail');
    this.cName = this.session.retrieve('cName');
    return this.authService.isAuthenticated();
  }

  isCurrentAdmin() {
    return this.roleGuardService.isCurrentAdmin();
  }
  isUserActive() {
    return this.authService.isUserActive();
  }
}
