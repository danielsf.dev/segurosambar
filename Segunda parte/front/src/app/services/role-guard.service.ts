import { Injectable } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { AuthService } from './auth.service';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(private session: SessionStorageService, public auth: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole;
    const token = this.session.retrieve('cToken');
    const tokenPayload = decode(token);
    if (!this.auth.isAuthenticated() || tokenPayload.data.role !== expectedRole) {
      this.router.navigate(['dashboard']);
      return false;
    }
    return true;
  }


  isCurrentAdmin() {
    const expectedRole = 'admin';
    const token = this.session.retrieve('cToken');
    const tokenPayload = decode(token);
    if (!this.auth.isAuthenticated() || tokenPayload.data.role !== expectedRole) {
      return false;
    }
    return true;
  }
}