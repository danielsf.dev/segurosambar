import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) { }



  canActivate(routeerstate?: any): boolean {
    var url = routeerstate._routerState.url;
    url = url.replace('/', '').replace('(', '').replace(')', '');
    if (!this.auth.isAuthenticated()) {
      if (url === 'login' || url.includes('signup') || url === '' || url === 'forgotpassword') {
        return true
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    }
    else {
      if (url === 'login' || url.includes('signup') || url === '' || url === 'forgotpassword') {
        this.router.navigate(['/dashboard']);
      } else {
        return true;
      }
    }
    return true;
  }

}