import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SessionStorageService } from 'ngx-webstorage';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private session: SessionStorageService,
    public jwtHelper: JwtHelperService,
    private http: HttpClient
  ) { }

  public isAuthenticated(): boolean {
    let token = this.session.retrieve('cToken');
    if (this.jwtHelper.isTokenExpired(token)) {
      this.session.store('isLogged', false);
      return false;
    }
    else {
      this.session.store('isLogged', true);
      return true;
    }
  }

  isUserActive() {
    const token = this.session.retrieve('cToken');
    const tokenPayload = decode(token);
    if (tokenPayload.data.nStatusPayment == 0)
      return false;
    else return true;
  }

  login(data) {
    return this.http.post(environment.baseUrl + "auth/signin", data);
  }

  signup(data) {
    return this.http.post(environment.baseUrl + "auth/signup", data);
  }


}