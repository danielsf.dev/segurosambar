import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionStorageService } from 'ngx-webstorage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(
    private http: HttpClient,
    private session: SessionStorageService
  ) { }

  headers: any = { headers: { 'Authorization': this.session.retrieve('cToken') } };

  getUsers() {
    return this.http.get(environment.baseUrl + "user", this.headers);
  }

  createUser(data: any) {
    return this.http.post(environment.baseUrl + "user", data, this.headers);
  }

  editUser(idUser: string, data: any) {
    return this.http.put(environment.baseUrl + "user/" + idUser, data, this.headers);
  }

  deleteUser(idUser: string) {
    return this.http.delete(environment.baseUrl + "user/" + idUser, this.headers);
  }
}
