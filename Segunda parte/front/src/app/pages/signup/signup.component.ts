import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SessionStorageService } from 'ngx-webstorage';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  constructor(
    public authService: AuthService,
    private fb: FormBuilder,
    public router: Router,
    private session: SessionStorageService,
    private notification: NzNotificationService
  ) { }

  validateForm: FormGroup;
  isLoading: boolean = false;

  submitForm(): void {
    this.isLoading = true;
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.controls.name.status === 'VALID' && this.validateForm.controls.email.status === 'VALID' && this.validateForm.controls.password.status === 'VALID') {

      let data = {
        name: this.validateForm.controls.name.value,
        email: this.validateForm.controls.email.value,
        pass: this.validateForm.controls.password.value
      };

      this.authService.signup(data).subscribe(
        (resp: any) => {
          this.notification.success('Éxito', resp.message);
          this.session.store('cToken', resp.token)
          this.session.store('isLogged', true)
          this.session.store('cEmail', resp.data.email);
          this.session.store('cName', resp.data.name);
          this.isLoading = false;
          this.router.navigate(['/dashboard']);
        }, err => {
          this.notification.error('Algo salio mal', err.error.message);
          this.isLoading = false;
        }
      );
    } else {
      this.isLoading = false;
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
}
